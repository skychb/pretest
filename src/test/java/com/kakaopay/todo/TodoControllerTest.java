package com.kakaopay.todo;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.mockito.Matchers.eq;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.mockito.Matchers.argThat;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kakaopay.controller.TodoController;
import com.kakaopay.model.Todo;
import com.kakaopay.repository.TodoRelationRepository;
import com.kakaopay.repository.TodoRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@WebMvcTest(controllers = TodoController.class)
public class TodoControllerTest {
    @Autowired
	private MockMvc mvc;
	
    @Autowired
    private ObjectMapper mapper;
	
    @MockBean
	private TodoRepository repository;
	
    @MockBean
	private TodoRelationRepository relationRepository;
    
	//생성 Test
	@Test
	public void testCreate() throws Exception{
		String requestBody = mapper.writeValueAsString(
                Todo.create("Test", "2018-01-01 00:00:00", "2018-01-01 00:00:00"));
		
		Todo response = new Todo(1, "Test", "2018-01-01 00:00:00", "2018-01-01 00:00:00", false, null);
		
		when(repository.save(any(Todo.class))).thenReturn(response);
		mvc.perform(post("/api/lists")
				.content(requestBody)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(content().json(mapper.writeValueAsString(response)));
		
		verify(repository).save(any(Todo.class));
	}
	
	//리스팅 Test
	@Test
	public void testList() throws Exception{
		List<Todo> results = Arrays.asList(new Todo(1, "Test", "2018-01-01 00:00:00", "2018-01-01 00:00:00", false, null));
		when(repository.findAll()).thenReturn(results);
		
		mvc.perform(get("/api/lists")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
	        .andExpect(content().json(mapper.writeValueAsString(results)));
	}
	
	//U
	@Test
    public void testUpdate() throws Exception {
		Todo todo = new Todo(1, "Test", "2018-01-01 00:00:00", "2018-01-01 00:00:00", false, null);
        when(repository.findOneById(1L)).thenReturn(todo);
        
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		String updateDt = fmt.print(new DateTime());
        Todo request = Todo.create("Test1", todo.getCreateDt(), updateDt);
        String requestBody = mapper.writeValueAsString(
                request);

        mvc.perform(put("/api/lists/{id}", 1L)
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());

        verify(repository).save(any(Todo.class));
    }

	
	//D
	@Test
    public void testDelete() throws Exception {
        mvc.perform(delete("/api/lists/{id}", 1L))
                .andDo(print());

        verify(repository).deleteById(1L);
    }

}
