package com.kakaopay.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TodoRequest {
	private long id;
	private String title;
	private String createDt;
	private String updateDt;
	private boolean completed;
	private String reference;

}
