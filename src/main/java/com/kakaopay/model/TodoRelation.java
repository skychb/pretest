/*
 * @(#)TodoRelation.java $version Mar 18, 2019
 *
 * Copyright 2019 Neptune. All rights Reserved. 
 * NEPTUNE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.kakaopay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "todo_relation")
public class TodoRelation {
	@Id
	@GeneratedValue
	private long id;
	@Column(name = "todo_id")
	private long todoId;
	@Column(name = "parent_id")
	private long parentId;
	
	public static TodoRelation create(long id, long todoId, long parentId){
		TodoRelation todoRelation = new TodoRelation();
		todoRelation.setId(id);
		todoRelation.setTodoId(todoId);
		todoRelation.setParentId(parentId);
		
		return todoRelation;
	}
}
