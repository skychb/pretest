package com.kakaopay.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@Table(name = "todo")
public class Todo implements Serializable {
	@Id
	@GeneratedValue
	private long id;
	private String title;
	private String createDt;
	private String updateDt;
	private boolean completed;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "todo_id")
	private List<TodoRelation> linked;
	
	public static Todo create(String title, String createDt, String updateDt){
		Todo todo = new Todo();
		todo.setTitle(title);
		todo.setCreateDt(createDt);
		todo.setUpdateDt(updateDt);
		return todo;
	}

	public Todo(long id, String title, String createDt, String updateDt, boolean completed, List<TodoRelation> linked) {
		super();
		this.id = id;
		this.title = title;
		this.createDt = createDt;
		this.updateDt = updateDt;
		this.completed = completed;
		this.linked = linked;
	}
	
	
}
