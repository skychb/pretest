package com.kakaopay.controller;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kakaopay.model.Todo;
import com.kakaopay.model.TodoRelation;
import com.kakaopay.model.TodoRequest;
import com.kakaopay.repository.TodoRelationRepository;
import com.kakaopay.repository.TodoRepository;

@RestController
@RequestMapping("/api")
public class TodoController {

	@Autowired
	private TodoRepository repository;
	
	@Autowired
	private TodoRelationRepository relationRepository;

	@PostMapping("/lists")
	public Todo create(@RequestBody TodoRequest todo) {
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		String createDt = fmt.print(new DateTime());
		todo.setCreateDt(createDt);
		todo.setUpdateDt(createDt);
		Todo result = repository.save(Todo.create(todo.getTitle(), createDt, createDt));

		if(!StringUtils.isEmpty(todo.getReference())){
			int[] splited = Arrays.asList(todo.getReference().split(",")).stream().mapToInt(Integer::parseInt).toArray();
			for(int i : splited){
				relationRepository.save(TodoRelation.create(0, result.getId(), i));
			}
		}
		
		return result;
	}

	@GetMapping("/lists")
	public List<Todo> list() {
		List<Todo> todos = repository.findAll();
		return todos;
	}

	@DeleteMapping("/lists/{id}")
	public void delete(@PathVariable("id") Long id) {
		repository.deleteById(id);
	}

	@PutMapping("/lists/{id}")
	public Todo update(@PathVariable("id") Long id, @RequestBody(required = false) Todo request) {
		Todo todo = repository.findOneById(id);
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		String updateDt = fmt.print(new DateTime());
		todo.setUpdateDt(updateDt);

		if (request == null) {
			todo.setCompleted(!todo.isCompleted());
		} else {
			todo.setTitle(request.getTitle());
		}
		return repository.save(todo);
	}

}
