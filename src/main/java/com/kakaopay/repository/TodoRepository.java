package com.kakaopay.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.kakaopay.model.Todo;
import com.kakaopay.model.TodoRequest;

public interface TodoRepository extends CrudRepository<Todo, Integer>{
	 List<Todo> findAll();
	 
    Todo findOneById(Long id);

    @Transactional
    void deleteById(Long id);
    
    @Transactional
    Todo save(TodoRequest todo);
}
