package com.kakaopay.repository;

import org.springframework.data.repository.CrudRepository;

import com.kakaopay.model.TodoRelation;

public interface TodoRelationRepository extends CrudRepository<TodoRelation, Integer>{

}
