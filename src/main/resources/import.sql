insert into todo(id, title, create_dt, update_dt, completed) values (1, 'Test List 1', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);
insert into todo(id, title, create_dt, update_dt, completed) values (2, 'Test List 2', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);
insert into todo(id, title, create_dt, update_dt, completed) values (3, 'Test List 3', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);
insert into todo(id, title, create_dt, update_dt, completed) values (4, 'Test List 4', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);
insert into todo(id, title, create_dt, update_dt, completed) values (5, 'Test List 5', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);
insert into todo(id, title, create_dt, update_dt, completed) values (6, 'Test List 6', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);
insert into todo(id, title, create_dt, update_dt, completed) values (7, 'Test List 7', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);
insert into todo(id, title, create_dt, update_dt, completed) values (8, 'Test List 8', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);
insert into todo(id, title, create_dt, update_dt, completed) values (9, 'Test List 9', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);
insert into todo(id, title, create_dt, update_dt, completed) values (10, 'Test List 10', '2018-01-01 00:00:00', '2018-01-01 00:00:00',0);

insert into todo_relation(id, todo_id, parent_id) values (1, 2, 1);
insert into todo_relation(id, todo_id, parent_id) values (2, 3, 1);
insert into todo_relation(id, todo_id, parent_id) values (3, 4, 1);
insert into todo_relation(id, todo_id, parent_id) values (4, 4, 3);