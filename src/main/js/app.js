'use strict';

const React = require('react');
const ReactDOM = require('react-dom');

const ListGroup = require('react-bootstrap').ListGroup;
const FormControl = require('react-bootstrap').FormControl;
const FormGroup = require('react-bootstrap').FormGroup;
const ControlLabel = require('react-bootstrap').ControlLabel;
const Button = require('react-bootstrap').Button;
const InputGroup = require('react-bootstrap').InputGroup;
const Checkbox = require('react-bootstrap').Checkbox;

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {todos: [], newTodoName: '', updatedTodoName: '', todoItemAdders: [], reference : ''};
        this.handleNewTodoNameChange = this.handleNewTodoNameChange.bind(this);
        this.handleUpdatedTodoNameChange = this.handleUpdatedTodoNameChange.bind(this);
        this.handleItemAdderNameChange = this.handleItemAdderNameChange.bind(this);
        this.handleReferenceChange = this.handleReferenceChange.bind(this);
        this.handleNewTodo = this.handleNewTodo.bind(this);
        this.handleDeleteTodo = this.handleDeleteTodo.bind(this);
        this.handleUpdateTodo = this.handleUpdateTodo.bind(this);
    }

    componentDidMount() {
        fetch('/api/lists/', {
            method: 'GET',
            credentials: 'same-origin'
        }).then(response => {
            return response.json();

        }).then(json => {
            this.setState({todos: json, todoItemAdders: new Array(json.length).fill(''), reference : ''});
        });
    }

    handleItemAdderNameChange(index, name) {
        this.setState(function (prevState, props) {
            let myTodoItemAdders = prevState.todoItemAdders;
            myTodoItemAdders[index] = name;
            return {
                todoItemAdders: myTodoItemAdders
            };
        });
    }

    handleNewTodoNameChange(listName) {
        this.setState({newTodoName: listName});
    }

    handleUpdatedTodoNameChange(listName) {
        this.setState({updatedTodoName: listName});
    }

    handleReferenceChange(reference){
        this.setState({reference : reference})
    }

    handleNewTodo() {
        let newList = {
            title: this.state.newTodoName,
            reference : this.state.reference
        };
        fetch('/api/lists', {
            method: 'POST',
            credentials: 'same-origin',
            body: JSON.stringify(newList),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }).then(response => {
            return response.json();
        }).then(json => {
            this.setState(function (prevState, props) {
                let list = this.state.reference.split(",");
                let listParent = [];
                list.map((elem) => {
                    listParent.push({todoId : json.id, parentId : elem});
                });
                json.linked = listParent;
                let myTodos = prevState.todos;
                myTodos.push(json);
                return {
                    todoLists: myTodoLists,
                    newTodoName: ''
                };
            });
        });
    }

    handleUpdateTodo(listId) {

        let updatedTodo = {
            title: this.state.updatedTodoName
        };

        fetch('/api/lists/' + listId, {
            method: 'PUT',
            credentials: 'same-origin',
            body: JSON.stringify(updatedTodo),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }).then(response => {
            this.setState(function (prevState, props) {
                let myTodos = prevState.todos;
                myTodos.forEach(function (todoList) {
                    if (todoList.id === listId) {
                        todoList.title = prevState.updatedListName;
                    }
                });
                return {
                    todos: myTodos,
                    updatedTodoName: ''
                };
            });
        });
    }

    handleDeleteTodo(listId) {
        fetch('/api/lists/' + listId, {
            method: 'DELETE',
            credentials: 'same-origin'
        }).then(response => {
            this.setState(function (prevState, props) {
                let myTodoLists = prevState.todoTodos.filter((todoList =>
                    todoList.id !== listId
                ));
                return {
                    todos: myTodoLists
                };
            });
        });
    }

    render() {
        const newTodoName = this.state.newTodoName;
        const reference = this.state.reference;

        return (
            <div>
                <Todos todos={this.state.todos}
                           updatedTodoName={this.state.updatedTodoName}
                           onDeleteTodo={this.handleDeleteTodo}
                           onUpdateTodo={this.handleUpdateTodo}
                           onTodoNameChange={this.handleUpdatedTodoNameChange}
                />
                <AddTodoTodo todoName={newTodoName}
                             reference={reference}
                             onAddTodo={this.handleNewTodo}
                             onTodoNameChange={this.handleNewTodoNameChange}
                             onReferenceChange={this.handleReferenceChange}/>
            </div>
        );
    }

}

class AddTodoTodo extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReferenceChange = this.handleReferenceChange.bind(this);
    }

    handleChange(e) {
        this.props.onTodoNameChange(e.target.value);
    }

    handleReferenceChange(e) {
        this.props.onReferenceChange(e.target.value);
    }

    handleClick() {
        this.props.onAddTodo();
    }

    handleSubmit(e) {
        e.preventDefault();
        this.handleClick();
    }

    render() {
        const todoName = this.props.todoName;
        const reference = this.props.reference;

        return (
            <h1>TODO awkward</h1>
            <form onSubmit={this.handleSubmit}>
                <FormGroup>
                    <ControlLabel>ADD NEW TODO</ControlLabel>
                    <FormControl
                        type="text"
                        value={todoName}
                        placeholder="Enter Tile"
                        onChange={this.handleChange}
                    />
                    <FormControl
                        type="text"
                        value={reference}
                        placeholder="Type id referenced like '1,2,3'"
                        onChange={this.handleReferenceChange}
                        />
                    <Button bsStyle="default" onClick={this.handleClick}>Add Todo</Button>
                </FormGroup>
            </form>
        );
    }
}

class Todos extends React.Component {
    constructor(props) {
        super(props);

        this.handleTodoNameClick = this.handleTodoNameClick.bind(this);
        this.handleItemCheckboxChange = this.handleItemCheckboxChange.bind(this);
        this.toggleEditingOff = this.toggleEditingOff.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleUpdateButtonClick = this.handleUpdateButtonClick.bind(this);

        this.state = {editing: '', todos : this.props.todos, updatedTodoName: ''};
    }

    toggleEditingOff() {
        this.setState({editing: ''});
    }

    handleTodoNameClick(listId, listName) {
        this.props.onListNameChange(listName);
        this.setState({editing: listId});
    }

    handleSubmit(e, objId) {
        e.preventDefault();
        this.handleUpdateButtonClick(objId);
    }

    handleNameChange(e) {
        this.props.onTodoNameChange(e.target.value);
    }

    handleUpdateButtonClick(objId) {
        this.props.onUpdateTodo(objId);
        this.toggleEditingOff();
    }

    handleItemCheckboxChange(listId) {
        let todoToToggle = this.props.todos.filter( item => item.id === listId)[0];
        let resultStackedArray = [];
        let resultStackingArray = [];

        this.props.todos.map(item => item.linked.forEach((elem) =>{
            if(elem.parentId == listId){
                resultStackedArray.push(elem.todoId);
            }
            if(elem.todoId == listId){
                resultStackingArray.push(elem.parentId);
            }
        }));
        let todoParentFiltered = this.props.todos.filter( item => resultStackedArray.includes(item.id) && item.completed == false);
        let todoParentFiltering = this.props.todos.filter( item => resultStackingArray.includes(item.id) && item.completed == true);

        if(todoParentFiltered.length != 0){
            var idString = "";
            todoParentFiltered.map((e, idx) => {
                if(idx != todoParentFiltered.length -1){
                    idString += e.id + ","
                }else{
                    idString += e.id
                }
            });
            alert("먼저 " + idString + "번 TODO를 수행하세요.");
            return;
        }

        if(todoParentFiltering.length != 0){
            var idString = "";
            todoParentFiltering.map((e, idx) => {
                if(idx != todoParentFiltering.length - 1){
                    idString += e.id + ","
                }else{
                    idString += e.id
                }
            });
            alert("먼저 " + idString + "번 TODO을 해제하세요.");
            return;
        }

        todoToToggle.completed = !todoToToggle.completed;

        fetch('/api/lists/' + listId, {
            method: 'PUT',
            credentials: 'same-origin',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }).then(response => {
            this.setState(function (prevState, props) {
                let myItems = prevState.lists;
                myItems.forEach(function (item) {
                    if (item.id === listId) {
                        item.completed = todoToToggle.completed;
                    }
                });
                return {
                    items: myItems,
                    updatedItemName: ''
                }
            });
        })
    }

    render() {
        const todoName = this.props.todoName;
        var todos = this.props.todos.map((list, index) =>
                {
                    var linkedText = "";
                    list.linked.map((link, idx) =>{
                        if(idx != list.linked.length -1){
                            linkedText += ("@" + link.parentId + ", ")
                        }else{
                            linkedText += ("@" + link.parentId)
                        }
                    });

                    if (this.state.editing === list.id) {
                        return (
                            <tr key={list.id}>
                                <td> {list.id} </td>
                                <td> <FormGroup>
                                    <InputGroup>
                                        <FormControl
                                            type="text"
                                            placeholder={list.title}
                                            value={listName}
                                            onChange={this.handleNameChange}
                                        />
                                        <InputGroup.Button>
                                            <Button onClick={() => this.handleUpdateButtonClick(list.id)}>Update</Button>
                                        </InputGroup.Button>
                                    </InputGroup>
                                </FormGroup></td>
                                <td> {list.createDt} </td>
                                <td> {list.updateDt} </td>
                                <td> <Checkbox checked={list.completed} onChange={() => this.handleItemCheckboxChange(list.id)}> </Checkbox> </td>
                                <td> {linkedText} </td>
                                <td><Button bsStyle="danger" onClick={() => this.props.onDeleteList(list.id)}>Delete</Button></td>
                            </tr>


                        );
                    } else {
                        return (
                            <tr key={list.id}>
                                <td> {list.id} </td>
                                <td> <p onClick={() => this.handleListNameClick(list.id, list.title)}>
                                    {list.title}
                                </p> </td>
                                <td> {list.createDt} </td>
                                <td> {list.updateDt} </td>
                                <td> <Checkbox checked={list.completed} onChange={() => this.handleItemCheckboxChange(list.id)}> </Checkbox> </td>
                                <td> {linkedText} </td>
                                <td><Button bsStyle="danger" onClick={() => this.props.onDeleteList(list.id)}>Delete</Button></td>
                            </tr>
                        )
                    }
                }
        );
        let obj = this.props.object;
        return (
            <form onSubmit={(e) => this.handleSubmit(e, obj.id)}>
                <table className="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>할일</th>
                            <th>작성일시</th>
                            <th>최종수정일시</th>
                            <th>완료</th>
                            <th>Linked</th>
                            <th>UTIL</th>
                        </tr>
                    </thead>
                    <tbody>
                    {todos}
                    </tbody>
                </table>
            </form>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('react')
);
